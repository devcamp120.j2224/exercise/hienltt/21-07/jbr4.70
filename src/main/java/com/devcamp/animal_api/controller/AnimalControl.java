package com.devcamp.animal_api.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.animal_api.model.*;

@RestController
public class AnimalControl {
    @CrossOrigin
    @GetMapping("/cats")
    public ArrayList<Cat> getListCat(){
        ArrayList<Cat> listCats = new ArrayList<>();
        Cat cat1 = new Cat("Sahra");
        Cat cat2 = new Cat("Bucky");
        Cat cat3 = new Cat("Lux");
        listCats.add(cat1);
        listCats.add(cat2);
        listCats.add(cat3);
        return listCats;
    }

    @GetMapping("/dogs")
    public ArrayList<Dog> getListDog(){
        ArrayList<Dog> listDogs = new ArrayList<>();
        Dog dog1 = new Dog("Lulu");
        Dog dog2 = new Dog("Ben");
        Dog dog3 = new Dog("Pom");
        listDogs.add(dog1);
        listDogs.add(dog2);
        listDogs.add(dog3);
        return listDogs;
    }
}
